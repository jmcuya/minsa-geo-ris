import { ISidebarMenu } from '../../../features/admin/interfaces/menu.interface';

export const options: ISidebarMenu[] = [
  {
    title: 'RIS- Perú',
    icon: 'bx-desktop',
    role: 'publico',
    childrens: [
      { title: 'Inicio', url: '/website/inicio' },
      {
        title: 'Intercambio <br> Prestacional',
        url: '/website/intercambio-prestacional',
      },
      {
        title: 'Resoluciones de <br> Conformación de RIS',
        url: '/website/resolucion-de-confirmacion-de-ris',
      },
      {
        title: 'Equipos  <br> Impulsores de <br> RIS',
        url: '/website/equipos-impulsores-de-ris',
      },
    ],
  },
  {
    title: 'Mapa RIS',
    icon: 'bx-map-alt',
    url: '/website',
  },
  {
    title: 'Dashboard',
    icon: 'bx-bar-chart-alt-2',
    url: '/website/dashboard',
  },

  /** ADD MENÚ ETAPAS   */

  //#region Etapa 0

  {
    title: 'Etapa 0: Definición de Política y Marco Normativo',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      {
        title: 'Fase 1: Política Sectorial RIS',
        icon: 'bx-map',
        role: 'publico',
        childrens: [
          {
            title: 'Política Nacional Multisectorial de Salud (DS)',
            url: '/etapas/etapas-0/fases/fase-1/Politica-Nacional-Multisectorial-Salud',
            icon: 'icon-drop',
          },
          {
            title: 'Política General de Gobierno',
            url: '/etapas/etapas-0/fases/fase-1/Politica-General-Gobierno',
            icon: 'icon-drop',
          },
          {
            title: 'Ley 30885',
            url: '/etapas/etapas-0/fases/fase-1/Ley30885',
            icon: 'icon-drop',
          },
        ],
      },
      {
        title: 'Fase 2:  Emisión del marco normativo de la Política Sectorial',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title:
              'Documentos normativos específicos para laregulación del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Especificos-Regularizacion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “prestación” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gestión” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Gestion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “financiamiento” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Financiamiento-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gobernanza” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Gobernanza-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title: 'Abogacía del Comité Impulsor Nacional de las RIS',
            url: '/etapas/etapas-0/fases/fase-2/Abogacia-Comite-Impulsor-Nacional-RIS',
            icon: 'icon-drop',
          },
          {
            title: 'RM del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/RM-COINRIS',
            icon: 'icon-drop',
          },
          {
            title: 'Plan de Trabajo del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Plan-Trabajo-COINRIS',
            icon: 'icon-drop',
          },
          {
            title: 'Actas del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Actas-COINRIS',
            icon: 'icon-drop',
          },
          {
            title: 'Informes del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Informes-COINRIS',
            icon: 'icon-drop',
          },
        ],
      },
      {
        title: 'Fase 3: Abogacía para la RIS',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title: 'Otra Opción 1',
            url: '/Etapa-0/Fase-3/Otra-Opcion-1',
            icon: 'icon-drop',
          },
          {
            title: 'Otra Opción 2',
            url: '/Etapa-0/Fase-3/Otra-Opcion-2',
            icon: 'icon-drop',
          },
        ],
      },
      {
        title: 'Fase 4: Adecuación del Marco Normativo',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title: 'Otra Opción 1',
            url: '/Etapa-0/Fase-4/Otra-Opcion-1',
            icon: 'icon-drop',
          },
          {
            title: 'Otra Opción 2',
            url: '/Etapa-0/Fase-4/Otra-Opcion-2',
            icon: 'icon-drop',
          },
        ],
      },
    ],
  },

  //#endregion Etapa 0

  //#region Etapa 1
  {
    title: 'Etapa 1:  Conformación de la RIS del MINSA y GORE',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      // {
      //   title: 'Cronograma',
      //   url: '/Etapa-0',
      //   icon: 'icon-drop'
      // },
      {
        title: 'Fase 1: Estructuración de las RIS',
        icon: 'bx-map',
        role: 'publico',
        childrens: [
          {
            title:
              'Avances (Mapa del Perú con los avances de la estructuración y resultados por RIS de Línea de Base)',
            url: '/Etapa-1/Fase-1/Avances-Estructuracion-Resultados-RIS-Linea-Base',
            icon: 'icon-drop',
          },
          {
            title:
              'Avances (Mapa del Perú con las RD de conformación de los EIRIS)',
            url: '/website/equipos-impulsores-de-ris',
            icon: 'icon-drop',
          },
          {
            title:
              'Mapa de Peru con Ambitos Priorizados por al Autoridad Sanitaria para Estructurar RIS',
            url: '/Etapa-1/Fase-1/Mapa-Peru-Ambitos-Priorizados-Autoridad-Sanitaria-Estructurar-RIS',
            icon: 'icon-drop',
          },
          {
            title: 'Herramientas',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Herramienta de Cartografía de estructuración de RIS',
                url: '/website',
                icon: 'icon-drop',
              },
              {
                title: 'Calculador de Tamaño Poblacional',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title:
                  'Matriz de Operacionalización de Criterios Técnicos para definir Unidades Territoriales Sanitarias Población e IPRESS Relacionadas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title:
                  'Instrumento de Medición de Grado de Integración de la RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title:
                  'FICHA DE VISITA A ESTABLECIMIENTO DE SALUD DEL SEGUNDO NIVEL DE ATENCIÓN DE SALUD PARA CORROBORAR INFORMACIÓN SOBRE MEDICIÓN DEL GRADO DE INTEGRACIÓN DE LA RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title:
                  'FICHA DE VISITA A ESTABLECIMIENTO DE SALUD DEL PRIMER NIVEL DE ATENCIÓN DE SALUD PARA CORROBORAR INFORMACIÓN SOBRE MEDICIÓN DEL GRADO DE INTEGRACIÓN DE LA RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
        ],
      },
      {
        title: 'Fase 2:  Formalización de la RIS',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title:
              'Avances (Mapa del Perú con las RD de conformación de las RIS)',
            url: '/website/resolucion-de-confirmacion-de-ris',
            icon: 'icon-drop',
          },
          {
            title: 'Herramientas',
            url: '/Etapa-0/Fase-2/Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title: 'Modelo del RD',
            url: '/Etapa-0/Fase-2/Componente4',
            icon: 'icon-drop',
          },
          {
            title: 'Modelo de Informe de Sustentación',
            url: '/Etapa-0/Fase-2/Componente5',
            icon: 'icon-drop',
          },
        ],
      },      
      {
        title: 'Dashboard',
        role: 'publico',
        icon: 'icon-drop',
        childrens: [
          {
            title: 'Dashboard',
            url: '/website/dashboard',
            icon: 'icon-drop',
          }
        ]
      }
    ],
  },
  //#endregion

  //#region Etapa 2
  {
    title: 'Etapa 2:  Desarrollo de las RIS del MINSA y GORE',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      // {
      //   title: 'Cronograma',
      //   url: '/Etapa-0',
      //   icon: 'icon-drop'
      // },
      {
        title: 'Fase 1: Desarrollo Inicial de las RIS',
        icon: 'bx-map',
        role: 'publico',
        childrens: [
          {
            title:
              'subfase 1 Elaboración del Plan de Fortalecimiento de la Capacidad Resolutiva de las RIS',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 2: Adecuación de documentos de gestión',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 3: Conformación y funcionamiento del EGRIS',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 4: Estandarización de procesos',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title:
              'subfase 5: Formulación, ejecución y control del Plan de Salud de las RIS',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 6: Implementación progresiva del SIHCE',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 7: Implementación de telesalud',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'Subfase 8: Fortalecimiento de la instancia de gobernanza',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 9: Incorporación de mecanismos de financiamiento',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 10: Optimización de recursos',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
        ],
      },
      {
        title: 'Fase 2: Desarrollo Gradual de las RIS',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title: 'subfase 1: Cierre Gradual de Brecha de EMS',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
          {
            title: 'subfase 2: Cierre Gradual de Cartera de Servicios de Salud',
            icon: 'icon-drop',
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                icon: 'icon-drop',
              },
            ],
          },
        ],
      },
    ],
  },
  //#endregion

  //#region Etapa 3
  {
    title: 'Etapa 3: Integración de otras IPRESS públicas, privadas o mixtas',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      {
        title: 'Fase 1: Proceso de Homologación para la integración',
        url: '/Etapa-0',
      },
      {
        title: 'Fase 2: Suscripción de convenios de integración a las RIS',
        url: '/Implementacion'
      },
    ],
  },
  //#endregion

  //#region Etapa 4
  {
    title: 'Etapa 4: Definición de Política y Marco Normativo',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      {
        title: 'Fase 1: Política Sectorial RIS',
        icon: 'bx-map',
        role: 'publico',
        childrens: [
          {
            title: 'Política Nacional Multisectorial de Salud (DS)',
            url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
            icon: 'icon-drop',
          },
          {
            title: 'Política General de Gobierno',
            url: '/Etapa-0/Fase-1/Politica-General-Gobierno',
            icon: 'icon-drop',
          },
          {
            title: 'Ley 30885',
            url: '/Etapa-0/Fase-1/Ley30885',
            icon: 'icon-drop',
          },
        ],
      },
      {
        title: 'Fase 2:  Emisión del marco normativo de la Política Sectorial',
        role: 'publico',
        //url: '/Implementacion',
        icon: 'icon-drop',
        childrens: [
          {
            title:
              'Documentos normativos específicos para laregulación del Modelo RIS',
            url: '/Etapa-0/Fase-2/Documentos-Normativos-Especificos-Regularizacion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “prestación” del Modelo RIS',
            url: '/Etapa-0/Fase-2/Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gestión” del Modelo RIS',
            url: '/Etapa-0/Fase-2/Componente4',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “financiamiento” del Modelo RIS',
            url: '/Etapa-0/Fase-2/Componente5',
            icon: 'icon-drop',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gobernanza” del Modelo RIS',
            url: '/Etapa-0/Fase-2/Componente6',
            icon: 'icon-drop',
          },
          {
            title: 'Abogacía del Comité Impulsor Nacional de las RIS',
            url: '/Etapa-0/Fase-2/Componente7',
            icon: 'icon-drop',
          }
        ],
      },
      {
        title: 'Fase 3: Abogacía para la RIS',
        url: '/Implementacion',
       
      },
      {
        title: 'Fase 4: Adecuación del Marco Normativo',
        url: '/Implementacion',
     
      },
    ],
  },
  //#endregion

  /** FIN MENÚ ETAPAS */

  {
    title: 'Ubica tu <br> Establecimiento <br> de Salud',
    icon: 'bx-map',
    role: 'publico',
    childrens: [
      { title: 'Todo Perú', url: '/admin/todo-peru-ris' },
      { title: 'Lima Metropolitana', url: '/admin/lima-metropolitana-ris' },
      { title: 'Tumbes', url: '/admin/tumbes-ris' },
      { title: 'Tacna', url: '/admin/tacna-ris' },
      { title: 'Madre de Dios', url: '/admin/madre-de-dios-ris' },
      { title: 'Ucayali', url: '/admin/ucayali-ris' },
    ],
  },
  {
    title: 'Nominalización',
    icon: 'bx-folder',
    role: '"[ROLE_MANAGER]"',
    childrens: [{ title: 'Búsqueda', url: '/admin/nominalizacion' }],
  },
  {
    title: 'Equipos Mult.',
    icon: 'bx-user-plus',
    role: '"[ROLE_MANAGER]"',
    childrens: [
      { title: 'Asignación', url: '/admin/equipos-multidiciplinario-de-salud' },
    ],
  },
];
