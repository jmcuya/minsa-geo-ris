import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, SessionService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  loading: boolean;
  message: string;
  show: boolean;

  constructor(
    private authHttp: AuthService,
    private fb: FormBuilder,
    private session: SessionService,
    private bsModal: BsModalService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  signIn(): void {
    if (this.form.valid) {
      this.authHttp.signIn(this.form.value)
        .subscribe(
          res => {
            this.loading = false;
            if (res) {
              this.session.create(res);
              this.bsModal.close(this.session.user);
            } else {
              this.message = 'El usuario/contraseña son incorrectos';
            }
          },
          (err) => {            
            this.loading = false;
            this.message = 'Credenciales incorrectas';
          }
        );
    }    
  }

}
