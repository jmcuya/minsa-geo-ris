export interface iNominalizationRequest {
    dni?: string;
    edad?: string;
    genero?: string;
    materno?: string;
    nombres?: string;
    option?: string;
    paterno?: string;
    quick?: string;
    ris?: string;
  }
  