import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-georismap',
  templateUrl: './georismap.component.html',
  styleUrls: ['./georismap.component.scss']
})
export class GeorismapComponent implements OnInit {

  @Input() item: any;
  url = "";
  urlSafe: SafeResourceUrl;
  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    console.log(this.item);
      this.url = "https://www.arcgis.com/apps/webappviewer/index.html?id=7da411b376504f3a8ee9c725b2301df4&marker="+this.item.longitud+";"+this.item.latitud+";;Ubicacion Nominal Aproximada <br> <br> <br> <strong>Nombres y Apellidos: </strong> "+this.item.nombres_ref + " "+ this.item.paterno_ref + " "+this.item.materno_ref+"<br> <strong>Edad: </strong>"+this.item.edad+" <br> <strong>Sexo: </strong>"+ this.item.genero+" <br> <strong>DNI: </strong> "+ this.item.dni +"<br><strong>IPRESS: </strong>"+this.item.ipress+"<br><strong>CODIGO RENI: </strong>"+this.item.cod_reni+"<br><strong>NUMERO DE RIS: </strong>"+this.item.id_ris+"<br><strong>IDMANZANA: </strong>"+this.item.idmanzana+";;DNI:"+this.item.dni+"&level=16";
      this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }


  clear(): void {

  }

}
