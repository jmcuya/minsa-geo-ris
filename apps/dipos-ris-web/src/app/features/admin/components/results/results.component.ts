import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';
import { of, throwError } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, switchMap, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  @Input() keywords: FormControl;

  @Output() selected = new EventEmitter<any>();

  results: any[] = [];

  form: FormGroup;

  sizePage: number = 12;

  page: number = 1;

  pages: number = 0;

  total: number = 0;

  isLoading: boolean;

  @ViewChild('perfectScroll', { static: false }) perfectScroll: ElementRef;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      keywords: this.keywords,
      sort: 'nombre'
    });

    this.watchKeywords();
  }

  watchKeywords(): void {
    this.form.valueChanges
      .pipe(
        tap(values => {
          this.page = 1;

          if (!values.keywords) {
            this.results = [];
            this.total = 0;
          }
        }),
        filter((values: any) => values.keywords?.length > 1),
        distinctUntilChanged(),
        tap(() => this.isLoading = true),
        debounceTime(400),
        // switchMap((values: any) => this.productService.search(values))
        switchMap((values: any) => {
          return this.productService.paging({ ...values, page: this.page, size: this.sizePage })
            .pipe(
              catchError(() => {
                this.isLoading = false;
                this.results = [];
                this.total = 0;
                this.pages = 0;
                return of();
              })
            )
        })
      )
      .subscribe((res: any) => {
        this.isLoading = false;
        if (res) {
          this.total = res.total;
          this.pages = res.pages;
          this.results = res.data;
        }

        this.perfectScroll.nativeElement.scrollTop = 0;
      });
  }

  track(event: Event) {
    if (!this.isLoading && this.page < this.pages) {
      const params = {
        ...this.form.value,
        page: ++this.page,
        size: this.sizePage
      }

      this.isLoading = true;

      this.productService.paging(params)
        .subscribe((res: any) => {
          this.isLoading = false;
          // this.total = res.total;
          // this.pages = res.pages;
          this.results = [...this.results, ...res.data];
        }, () => {
          this.isLoading = false;
        });
    }
  }

  trackById(index: number, item: any) {
    return item.IdProducto;
  }

  select(product: any): void {
    this.selected.emit(product);
  }

  loadImage(event: any): void {
    event.path[1].classList.remove('img-loading');
  }

  errorImage(event: any): void {
    event.onerror = null;
    event.target.src = './assets/no-photos.png';
  }

}
