import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NominalizacionComponent } from './nominalizacion.component';

describe('NominalizacionComponent', () => {
  let component: NominalizacionComponent;
  let fixture: ComponentFixture<NominalizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NominalizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NominalizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
