import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../../../../../../libs/dipos-ris-web-commons/src/lib/http/product.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BsModalService } from '../../shared/services/bs-modal.service';
import { GeorismapComponent } from '../../components/georismap/georismap.component';

@Component({
  selector: 'app-nominalizacion',
  templateUrl: './nominalizacion.component.html',
  styleUrls: ['./nominalizacion.component.scss']
})
export class NominalizacionComponent implements OnInit {

  formSearch: FormGroup;
  form: FormGroup;
  quick_label: String = 'Nombre de RIS:';
  ris = new FormControl();
  esris = [];
  paginationController: any = {};
  orders: any[] = [];
  isLoading: boolean;
  changeFilter: boolean = true;
  url = "";
  urlSafe: SafeResourceUrl;
  constructor( 
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private productService: ProductService,
    public sanitizer: DomSanitizer) { 
    this.builder();
  }

  ngOnInit() {
    this.formSearch.get('quick').setValue(1);
    this.productService.trademarks().subscribe(res => {
      this.esris = res;
    });
    // this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      option: 1,
      quick: null,
      ris: null,
      genero: null,
      dni: null,
      nombres: null,
      paterno: null,
      materno: null,
      edad: null
    });

    this.form = this.fb.group({
      id_order: null,
      client: null,
      id_state: null,
      id_employee: null,
      employee: null,
      date_start: null,
      date_end: null
    });
  }

  onChange($event){
    const valor = $event.target.value;
    if(valor == '1') {
      this.quick_label = 'Nombre de RIS:';
      this.changeFilter = true;
    }else if(valor == '2') {
      this.quick_label = 'Nº de RIS:';
      this.changeFilter = false;
    }
    this.productService.trademarks().subscribe(res => {
      this.esris = res;
    });
  }

  search() {
  
    let typeFilter = this.changeFilter?1:2;
    this.productService.resultados(this.formSearch.value,typeFilter).subscribe(res => {
      this.orders = res;
   
    });
  }

  clean() {
    this.orders = [];
    this.formSearch.get('quick').setValue(1);
    this.formSearch.get('ris').setValue(null);
    this.formSearch.get('dni').setValue(null);
    this.formSearch.get('nombres').setValue(null);
    this.formSearch.get('paterno').setValue(null);
    this.formSearch.get('materno').setValue(null);
    this.formSearch.get('genero').setValue(null);
    this.formSearch.get('edad').setValue(null);
    this.quick_label = 'Nombre de RIS:';
    this.changeFilter = true;
    this.url = "";
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  viewMap(item) {
    this.bsModal.open(GeorismapComponent, { item }).then(res => {
     // this.search();
    });
  }

}
