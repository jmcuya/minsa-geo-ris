import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaymentService } from '@dipos-ris-web-commons';
import { CustomerListComponent } from '../../../../../customers/components/customer-list/customer-list.component';

@Component({
  selector: 'app-payment-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class PaymentSearchComponent implements OnInit {

  formSearch: FormGroup;
  form: FormGroup;
  paginationController: any = {};
  payments: any[] = [];
  isLoading: boolean;

  constructor(
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private paymentService: PaymentService
  ) {
    this.builder();
    this.quickSearch();
  }

  ngOnInit() {
    this.formSearch.get('quick').setValue(7);
    // this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      option: 1,
      quick: null,
    });

    this.form = this.fb.group({
      id_payment: null,
      payment_type: 'PAGO REALIZADO',
      id_provider: null,
      provider: [{ value: null, disabled: true }],
      id_state: null,
      id_employee: null,
      employee: null,
      date_start: null,
      date_end: null
    });
  }

  search(): void {
    this.removeValidators();

    const values = this.form.getRawValue();

    if (!values.id_payment) {
      this.form.get('date_start').setValidators(Validators.required);
      this.form.get('date_end').setValidators(Validators.required);
      this.form.get('date_start').updateValueAndValidity();
      this.form.get('date_end').updateValueAndValidity();
    }

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this.isLoading = true;
    this.paymentService.search(values).subscribe(res => {
      this.payments = res;
      this.isLoading = false;
    }, () => this.isLoading = false);
  }

  dateTo(days: number): Date {
    return new Date(new Date().getTime() - days * 24 * 60 * 60 * 1000);
  }

  quickSearch(): void {
    this.formSearch.get('quick').valueChanges.subscribe(value => {
      let start = new Date();
      let end = new Date();

      switch (+value) {
        case 2:
        case 7:
        case 15:
        case 30:
          start = this.dateTo(+value);
          break;
        case 365:
          start = new Date(end.getFullYear(), 0, 1);
          break;
      }

      this.form.patchValue({
        date_start: start,
        date_end: end
      });

      this.search();
    });
  }

  removeValidators() {
    for (const key in this.form.controls) {
      this.form.get(key).clearValidators();
      this.form.get(key).updateValueAndValidity();
    }
  }

  reset(): void {
    const values = this.form.getRawValue();
    this.form.reset({
      payment_type: 'PAGO REALIZADO',
      date_start: values.date_start,
      date_end: values.date_end
    });
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  customerSearch(): void {
    this.bsModal.open(CustomerListComponent).then(res => {
      if (res) {
        this.form.patchValue({
          id_provider: res.IdPropietario,
          provider: res.RazonSocial
        });
      }
    });
  }

}
