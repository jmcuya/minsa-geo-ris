import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OwnerService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-customer-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class CustomerSearchComponent implements OnInit {

  formSearch: FormGroup;
  data: any[] = [];
  paginationController: any = {};

  constructor(
    private fb: FormBuilder,
    private ownerService: OwnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.builder();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      let type = params.filter;
      if (type) {
        type = type === 'clientes' ? 'CLI' : 'PRO';
      }

      this.formSearch.get('type').setValue(type);
      this.search();
    });
  }

  builder(): void {
    this.formSearch = this.fb.group({
      type: null,
      param_type: null,
      param_text: null
    });
  }

  search(): void {
    const params = this.formSearch.value;
    this.ownerService.getAll(params).subscribe(res => {
      this.data = res;
    });
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  edit(item: any): void {

  }

}
