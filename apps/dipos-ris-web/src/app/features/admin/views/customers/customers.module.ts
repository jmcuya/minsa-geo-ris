import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersComponent } from './customers.component';
import { CustomerSearchComponent } from './views/search/search.component';
import { CustomersRoutingModule } from './customers-routing.module';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { CustomerRegisterComponent } from './views/register/register.component';

@NgModule({
  imports: [
    // CommonModule,
    CustomersRoutingModule,
    AdminSharedModule
  ],
  declarations: [
    CustomersComponent,
    CustomerSearchComponent,
    CustomerRegisterComponent
  ]
})
export class CustomersModule { }
