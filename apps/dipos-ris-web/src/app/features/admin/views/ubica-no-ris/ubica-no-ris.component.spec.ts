import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicaNoRisComponent } from './ubica-no-ris.component';

describe('UbicaNoRisComponent', () => {
  let component: UbicaNoRisComponent;
  let fixture: ComponentFixture<UbicaNoRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UbicaNoRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicaNoRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
