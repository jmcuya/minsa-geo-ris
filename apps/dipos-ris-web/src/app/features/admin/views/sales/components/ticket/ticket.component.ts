import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TicketComponent implements OnInit {

  // @Input() ticket: any;
  // current = new Date();

  @Input() blobUrl: any;

  constructor() { }

  ngOnInit() {
  }

}
