import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesComponent } from './sales.component';
import { SalesRegisterComponent } from './views/register/register.component';
import { SalesSearchComponent } from './views/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: SalesComponent,
    children: [
      {
        path: '',
        component: SalesSearchComponent
      },
      {
        path: 'registro',
        component: SalesRegisterComponent
      },
      {
        path: 'registro/:id',
        component: SalesRegisterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }