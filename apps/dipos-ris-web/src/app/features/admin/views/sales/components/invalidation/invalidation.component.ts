import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MoneyboxService, OrderService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-invalidation',
  templateUrl: './invalidation.component.html',
  styleUrls: ['./invalidation.component.scss']
})
export class InvalidationComponent implements OnInit {
  @Input() order: any;
  form: FormGroup;
  boxes: any[];

  constructor(
    private boxService: MoneyboxService,
    private orderService: OrderService,
    private fb: FormBuilder,
    private bsModal: BsModalService
  ) {
    this.build();
  }

  ngOnInit() {
    this.getBoxes();
    this.getMovements();
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    this.form = this.fb.group({
      IdCaja: null,
      Pagado: 0,
      Devolver: [0, [onlyNumbers]]
    });
  }

  getBoxes(): void {
    this.boxService.getAll(2).subscribe(res => {
      this.boxes = res;

      this.form.get('IdCaja').setValue(res[0]?.IdCaja);
    });
  }

  getMovements(): void {
    this.boxService.getMovements({ reference: 'PEDIDO', id_reference: this.order.IdPedido })
      .subscribe(res => {
        const total = Math.round(res.reduce((a, m) => a + m.Importe * (m.TipoMovimiento == 'E' ? 1 : -1), 0) * 100) / 100;
        this.form.get('Pagado').setValue(total);
        this.form.get('Devolver').setValue(total);
      });
  }

  save(): void {
    const values = this.form.getRawValue();
    const order = this.order;
    const products = order.PedidoDetalle;

    const movements = products.map(item => ({
      IdAlmacen: item.IdAlmacen,
      TipoMovimiento: "E",
      IdMotivo: 8, //DEVOLUCION
      Fecha: new Date(),
      IdProducto: item.IdProducto,
      Cantidad: item.Cantidad,
      IdUnidadMedida: item.IdUnidadMedida,
      IdMoneda: order.IdMoneda,
      Costo: item.CostoUndBase,
      Precio: item.Precio,
      Observaciones: `PEDIDO N° ${order.IdPedido.toString().padStart(7, '0')}`
    }));

    this.orderService.invalidation(order, movements, values.IdCaja, values.Devolver).subscribe(res => {
      order.IdEstado = 3;
      this.bsModal.close({ order });
    });

  }

}
