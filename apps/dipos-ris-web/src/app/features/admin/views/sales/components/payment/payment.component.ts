import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MoneyboxService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @Input() order: any;

  form: FormGroup;
  cashes: any[];
  bills: number[] = [1, 2, 5, 10, 20, 50, 100, 200];
  isLoadingBoxes: boolean;

  constructor(
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private moneyboxService: MoneyboxService,
    private toastr: ToastrService
  ) {
    this.build();
  }

  ngOnInit() {
    this.form.patchValue({
      IdPedido: this.order?.IdPedido,
      Total: this.order?.TotalAjuste,
      Importe: this.order?.TotalAjuste,
      Saldo: 0
    });

    this.load();
    this.getMovements();
    this.calculateAmounts();
    this.watchAmount();
  }

  build(): void {
    this.form = this.fb.group({
      IdPedido: null,
      IdCaja: null,
      IdMoneda: null,
      Total: 0,
      TotalCobrado: 0,
      Importe: 0,
      Saldo: 0,
      TotalRecibido: 0,
      Vuelto: 0,
      Observaciones: null
    });
  }

  load(): void {
    this.isLoadingBoxes = true;

    // Cajas aperturadas
    this.moneyboxService.getAll(2).subscribe(res => {
      this.cashes = res;
      this.isLoadingBoxes = false;

      if (res.length) {
        this.cashChange(res[0]);
      } else {
        this.toastr.warning('No hay ninguna caja aperturada para cobrar', 'Aperture una caja');
      }
    }, () => {
      this.isLoadingBoxes = false;
    });
  }

  getMovements(): void {
    const form = this.form.getRawValue();
    const params = {
      reference: 'PEDIDO',
      id_reference: form.IdPedido
    };

    this.moneyboxService.getMovements(params).subscribe(res => {
      const form = this.form.getRawValue();
      const payment = res.filter(x => x.TipoMovimiento === 'E').reduce((a, b) => a + b.Importe, 0);
      let balance = form.Total - payment;
      if (balance < 0) balance = 0;

      this.form.patchValue({
        TotalCobrado: payment,
        Importe: balance,
        Saldo: balance
      });
    });
  }

  watchAmount(): void {
    this.form.valueChanges.subscribe(() => {
      this.calculateAmounts();
    });
  }

  calculateAmounts(): void {
    const form = this.form.getRawValue();

    const importe = form.Importe;
    const amount = form.TotalRecibido;
    const devolution = Math.round((amount > importe ? amount - importe : 0) * 100) / 100;

    this.form.get('Vuelto').setValue(devolution, { emitEvent: false });
  }

  selectAmount(amount: number): void {
    const current = +this.form.get('TotalRecibido').value || 0;

    this.form.get('TotalRecibido').setValue(current + amount);
  }

  resetAmount(): void {
    this.form.get('TotalRecibido').setValue(0);
  }

  setAmountTotal(): void {
    const importe = this.form.get('Importe').value;
    this.form.get('TotalRecibido').setValue(importe);
  }

  cashChange(item: any): void {
    this.form.patchValue({
      IdCaja: item.IdCaja,
      IdMoneda: item.IdMoneda
    });
  }

  saveAndClose(): void {
    const form = this.form.getRawValue();
    this.createMov(form).then(res => {
      if (res.Ok) {
        this.bsModal.close();
        this.toastr.success(res.Mensaje);
      } else {
        this.toastr.warning(res.Mensaje);
      }
    });
  }

  collectAndPrint(): void {
    const form = this.form.getRawValue();
    this.createMov(form).then(res => {
      if (res.Ok) {
        console.log('Imprimir!!!');
      } else {
        this.toastr.warning(res.Mensaje);
      }
    });
  }

  collectAndPayment(): void {
    const form = this.form.getRawValue();
    this.createMov(form).then(res => {
      if (res.Ok) {
        const new_balance = Math.round((form.Saldo - form.Importe) * 100) / 100;
        const total_payment = Math.round((+form.TotalCobrado + +form.Importe) * 100) / 100;

        this.form.get('Importe').setValue(new_balance);
        this.form.get('TotalCobrado').setValue(total_payment);
        this.form.get('Saldo').setValue(new_balance);
        this.form.get('TotalRecibido').setValue(0);
        this.form.get('Observaciones').reset();
        this.toastr.success(res.Mensaje);
      } else {
        this.toastr.warning(res.Mensaje);
      }
    });
  }

  createMov(form: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const movement = {
        IdCaja: form.IdCaja,
        IdConcepto: 3,
        Referencia: "PEDIDO",
        IdReferencia: this.order.IdPedido,
        IdMoneda: form.IdMoneda,
        Importe: form.Importe,
        FechaOperacion: new Date(),
        Observaciones: form.Observaciones,
      };

      const order = {
        ...this.order,
        Saldo: form.Saldo
      };

      this.moneyboxService.movementInsert({ movement, order }).subscribe(res => {
        resolve(res);
      }, () => {
        reject();
      })
    });
  }

}
