import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { WarehouseService } from '@dipos-ris-web-commons';
import Swal from 'sweetalert2';
import { TransferComponent } from '../transfer/transfer.component';

@Component({
  selector: 'app-stocktacking',
  templateUrl: './stocktacking.component.html',
  styleUrls: ['./stocktacking.component.scss']
})
export class StocktackingComponent implements OnInit, OnChanges {

  @Input() product: any;
  @Input() prices: boolean;

  @Output() selected = new EventEmitter();

  warehouse: FormControl = new FormControl();
  warehouses: any[] = [];
  presentations: any[] = [];

  constructor(
    private warehouseService: WarehouseService,
    private bsModalService: BsModalService,
    private fb: FormBuilder
  ) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.product.currentValue?.IdProducto != changes.product.previousValue?.IdProducto) {
      this.load();
    }

  }

  ngOnInit() {
    this.warehouse.valueChanges.subscribe(_ => this.changeWarehouse());
  }

  load() {
    console.log('Cargando inventario del producto', this.product?.IdProducto);
    this.warehouseService.getAll().subscribe(res => {
      this.warehouses = res;
      this.warehouse.setValue(res[0].IdAlmacen);
    });
  }

  changeWarehouse(): void {
    this.warehouseService.presentations(this.warehouse.value, this.product.IdProducto).subscribe(res => {

      const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);
      res.forEach(item => {
        const stock = Math.round(item.Stock * 1000) / 1000;
        item.Stock = stock;
        item.warehouses = this.warehouses.filter(x => x.IdAlmacen != item.IdAlmacen);

        item.transfer = this.fb.group({
          IdSource: this.warehouse.value,
          IdTarget: [item.warehouses[0]?.IdAlmacen, Validators.required],
          Stock: stock,
          IdUnidadMedida: item.IdUnidadMedida,
          Quantity: [1, [Validators.required, onlyNumbers]]
        });

        item.transfer.addValidators(this.fnValidatorQty());
      });

      this.presentations = res;
    });
  }

  fnValidatorQty(): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } | null => {
      const stock = group.get('Stock').value;
      const qty = group.get('Quantity').value;
      if (qty > stock) {
        group.get('Quantity').setErrors({ qtyExceed: true });
      }

      return null;
    }
  }

  transfer(item: any): void {
    const values = item.transfer.value;

    const source = {
      IdAlmacen: values.IdSource,
      IdProducto: item.IdProducto,
      IdUnidadMedida: item.IdUnidadMedida,
      Cantidad: item.Stock
    }

    const target = {
      IdAlmacen: values.IdTarget,
      IdProducto: item.IdProducto,
      IdUnidadMedida: item.IdUnidadMedida,
      Cantidad: values.Quantity
    }

    this.warehouseService.transfer(source, target).subscribe(res => {
      if (res.Ok) {
        this.changeWarehouse();

        Swal.fire({
          title: 'Transferido',
          text: 'El stock ha sido transferido exitosamente',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        });
      } else {
        Swal.fire({
          title: 'Atención',
          text: res.Mensaje,
          confirmButtonText: 'Aceptar',
          icon: 'error'
        });
      }
    });
  }

  /* stockTransfer(item: any): void {
    this.bsModalService.open(TransferComponent, { product: item }).then(res => {
      if (res) {
        this.changeWarehouse();
      }
    });
  } */

  selectedItem(item: any) {
    this.selected.emit(item);
  }

}
