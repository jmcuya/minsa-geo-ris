import { Location } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from '@dipos-ris-web-commons';
import { forkJoin, merge } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  categories: any[];
  trademarks: any[];
  sort: any[] = [
    { value: 'nombre', text: 'Nombre' },
    { value: 'categoria', text: 'Categoría' },
    { value: 'precio_asc', text: 'Precio (Menor a mayor)' },
    { value: 'precio_desc', text: 'Precio (Mayor a menor)' },
  ];
  sortText: string;

  form!: FormGroup;
  products: any[] = [];
  pagination = {
    size: 24,
    page: 1,
    total_pages: 0,
    total: 0
  }

  isLoading: boolean;

  constructor(
    private productsService: ProductService,
    private fb: FormBuilder,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.fb.group({
      category: '',
      trademark: '',
      keywords: '',
      sort: 'nombre'
    });
  }

  ngOnInit() {
    this.loadSelect();

    this.activatedRoute.queryParamMap.subscribe((paramMap: any) => {
      this.setFilters(paramMap.params);
      this.search();
    });

    merge(
      this.form.get('category').valueChanges,
      this.form.get('trademark').valueChanges,
      this.form.get('sort').valueChanges
    ).subscribe(values => {
      setTimeout(() => {
        this.submit();
      }, 1);
    });
  }

  loadSelect(): void {
    forkJoin([
      this.productsService.categories(),
      // this.productsService.trademarks()
    ]).subscribe(results => {
      this.categories = [{ IdCategoria: '', Detalle: 'Todas las categorías' }, ...results[0]];
      // this.trademarks = results[1];
    });
  }

  sortName(): string {
    return this.sort.find(x => x.value == this.form.get('sort').value)?.text;
  }

  search(): void {
    const params = {
      size: this.pagination.size,
      page: this.pagination.page,
      ...this.form.value
    };

    this.sortText = this.sortName();

    this.isLoading = true;

    this.productsService.paging(params)
      .subscribe((res: any) => {
        this.isLoading = false;
        this.products = res.data;

        this.pagination.total = res.total;
        this.pagination.total_pages = res.pages;
        this.managerUrl();
      }, () => {
        this.isLoading = false;

        // this.pagination.total = 0;
        // this.pagination.total_pages = 0;
      });
  }

  submit(): void {
    this.pagination.page = 1;

    this.search();
  }

  reset(): void {
    this.form.reset({ sort: 'nombre' }, { emitEvent: false });
    this.submit();
  }

  previousPage(): void {
    if (this.pagination.page > 1) {
      this.goToPage(this.pagination.page - 1);
    }
  }

  nextPage(): void {
    if (this.pagination.page < this.pagination.total_pages) {
      this.goToPage(this.pagination.page + 1);
    }
  }

  goToPage(page: number): void {
    this.pagination.page = page;
    this.search();
  }

  errorImage(event: any): void {
    event.onerror = null;
    event.target.src = './assets/no-photos.png';
  }

  setFilters(params: Params) {
    if (params.page) {
      this.pagination.page = +params.page;
    }

    this.form.patchValue(params);
  }

  managerUrl() {
    let params = new HttpParams();
    const values = {
      page: this.pagination.page,
      ...this.form.value
    };

    const remove = [['page', 1], ['sort', 'nombre'], ['category', '']];
    remove.forEach(val => {
      const key = val[0], value = val[1];
      if (values[key] === value) {
        delete values[key];
      }
    });

    Object.keys(values)
      .filter(key => !!values[key])
      .forEach(key => {
        params = params.set(key, values[key]);
      });

    // Para no invocar a router.events
    this.location.go('/inventario/productos', params.toString());
  }

}
