import { NgModule } from '@angular/core';
import { InventoryComponent } from './inventory.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { ProductsComponent } from './views/products/products.component';
import { RegisterComponent } from './views/register/register.component';
import { UnitAddComponent } from './components/unit-add/unit-add.component';
import { TrademarkComponent } from './components/trademark/trademark.component';
import { StocktackingComponent } from './components/stocktacking/stocktacking.component';
import { TransferComponent } from './components/transfer/transfer.component';

@NgModule({
  imports: [
    InventoryRoutingModule,
    NgSelectModule,
    AdminSharedModule
  ],
  declarations: [
    InventoryComponent,
    ProductsComponent,
    RegisterComponent,
    UnitAddComponent,
    TrademarkComponent,
    StocktackingComponent,
    TransferComponent
  ]
})
export class InventoryModule { }
