import { ChartOptions } from '@admin-shared/types/chart-type';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { StatisticsService } from '@dipos-ris-web-commons';
// import * as ApexCharts from 'apexcharts';
import { ChartComponent } from 'ng-apexcharts';
import { BarOptions } from './constants/chart-bar.constants';
import { DonutOptions } from './constants/donut-chart.constants';
import { SalesOptions } from './constants/spark-sales.constants';
import { ShoppingOptions } from './constants/spark-shopping.constants';
import { UtilitiesOptions } from './constants/spark-utilities.constants';

@Component({
  selector: 'app-statistics-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class StatisticsSalesComponent implements OnInit {
  @ViewChild("salesChart") salesChart: ChartComponent;
  @ViewChild("shoppingChart") shoppingChart: ChartComponent;
  @ViewChild("utilitiesChart") utilitiesChart: ChartComponent;
  @ViewChild("barChart") barChart: ChartComponent;
  @ViewChild("donutChart") donutChart: ChartComponent;

  public salesOptions: Partial<ChartOptions> = SalesOptions;
  public shoppingOptions: Partial<ChartOptions> = ShoppingOptions;
  public utilitiesOptions: Partial<ChartOptions> = UtilitiesOptions;
  public barOptions: Partial<ChartOptions> = BarOptions;
  public donutOptions: Partial<ChartOptions> = DonutOptions;

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private statisticsService: StatisticsService
  ) {
    this.builder();
  }

  builder(): void {
    var today = new Date();

    let date_start = new Date(today.getFullYear(), today.getMonth(), 1);
    let date_end = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    // let date_start = new Date(today.getTime() - 30 * 24 * 60 * 60 * 1000);

    this.form = this.fb.group({
      month: [today, Validators.required],
      date_start: [date_start],
      date_end: [date_end]
    });

    this.form.get('month').valueChanges.subscribe(date => {
      if (!date) return;

      let start = new Date(date.getFullYear(), date.getMonth(), 1);
      var end = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      this.form.get('date_start').setValue(start);
      this.form.get('date_end').setValue(end);
    });

    // this.form.setValidators(this.fnValidatorRangeDate());
  }

  /* fnValidatorRangeDate(): ValidatorFn {
    return (group: FormGroup): ValidationErrors | null => {
      const date_start = group.get('date_start').value;
      const date_end = group.get('date_end').value;

      group.get('date_start').setErrors(null);
      group.get('date_end').setErrors(null);

      if (!date_start) {
        group.get('date_start').setErrors({ invalid: true });
        return null;
      }
      if (!date_end) {
        group.get('date_end').setErrors({ invalid: true });
        return null;
      }

      const max = 3 * 30 * 24 * 60 * 60 * 1000;
      const diff = date_end.getTime() - date_start.getTime();
      // console.log(max, diff, diff > max);

      if (diff > max) {
        group.get('date_start').setErrors({ rangeMax: true });
        group.get('date_end').setErrors({ rangeMax: true });
      }

      return null;
    }
  } */

  ngOnInit() {
    this.search();
  }

  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  search(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
      return;
    }

    const range = this.form.getRawValue();
    const date_start = range.date_start.toISOString();
    const date_end = range.date_end.toISOString();

    this.drawChartSales(date_start, date_end);
    this.drawChartShopping(date_start, date_end);
    this.drawChartBar(date_start, date_end);
    this.drawChartDonut(date_start, date_end);
  }

  drawChartSales(date_start, date_end): void {
    this.statisticsService.sales(date_start, date_end).subscribe(res => {
      // Ventas
      const series = [{
        name: "Ventas",
        color: '#4723d9',
        data: res.map(value => ([value.Fecha, Math.round(value.Ventas * 100) / 100])) as any[]
      }];

      this.salesChart.updateSeries(series);

      const total_sales = (Math.round(res.reduce((a, b) => a + b.Ventas, 0) * 100) / 100).toLocaleString();
      this.salesChart.updateOptions({ title: { text: `S/ ${total_sales}` } });

      // Utilidades      
      this.drawChartUtilities(res);
    });
  }

  drawChartShopping(date_start, date_end): void {
    this.statisticsService.buys(date_start, date_end).subscribe(res => {

      const dates = this.getDates(new Date(date_start), new Date(date_end));
      const data = dates.map(date => {
        const d = date.toLocaleDateString();
        const t = res.find(x => new Date(x.Fecha).toLocaleDateString() == d)?.Total || 0;

        return [date.toISOString(), Math.round(t * 100) / 100] as any
      })

      const series = [{
        name: 'Compras',
        data//: res.map(value => ([value.Fecha, Math.round(value.Total * 100) / 100])) as any[]
      }];

      this.shoppingChart.updateSeries(series);
      const total_buys = (Math.round(res.reduce((a, b) => a + b.Total, 0) * 100) / 100).toLocaleString();
      this.shoppingChart.updateOptions({ title: { text: `S/ ${total_buys}` } });
    });
  }

  getDates(date_start: Date, date_end: Date): Date[] {
    const day = 24 * 60 * 60 * 1000;
    const total_days = Math.ceil((date_end.getTime() - date_start.getTime()) / day);
    return Array.from({ length: total_days }, (_, k) => (new Date(date_start.getTime() + k * day)))
  }

  drawChartUtilities(res: any[]): void {
    const data = res.map(value => ([value.Fecha, Math.round(value.Utilidad * 100) / 100])) as any[];

    const series = [{
      name: 'Utilidades',
      data
    }];

    this.utilitiesChart.updateSeries(series);

    const total_utilities = (Math.round(res.reduce((a, b) => a + b.Utilidad, 0) * 100) / 100).toLocaleString();
    this.utilitiesChart.updateOptions({ title: { text: `S/ ${total_utilities}` } });
  }

  drawChartBar(date_start, date_end): void {
    this.statisticsService.salesByHorary(date_start, date_end).subscribe(res => {
      const series = [{
        name: "Día",
        data: res.map(value => ([value.Fecha, Math.round(value.Total_Dia * 100) / 100])) as any[]
      }, {
        name: "Noche",
        data: res.map(value => ([value.Fecha, Math.round(value.Total_Noche * 100) / 100])) as any[]
      }];

      this.barChart.updateSeries(series);
    });
  }

  drawChartDonut(date_start, date_end): void {
    this.statisticsService.salesByCategory(date_start, date_end).subscribe(res => {
      const series = res.map(x => x.Total);
      const labels = res.map(x => x.Categoria);

      this.donutChart.updateOptions({ labels }, true, true, true);
      this.donutChart.updateSeries(series);
    });
  }

}
