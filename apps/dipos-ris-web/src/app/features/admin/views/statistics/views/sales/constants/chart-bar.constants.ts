import { ChartOptions } from "@admin-shared/types/chart-type";

export const BarOptions: Partial<ChartOptions> = {
  chart: {
    type: 'bar',
    height: 400,
    width: '100%',
    stacked: true,
  },
  plotOptions: {
    bar: {
      columnWidth: '45%',
    }
  },
  colors: ['#00e396d9', '#4723d9d9'],
  series: [],
  // labels: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
  dataLabels: {
    enabled: false
  },
  xaxis: {
    type: 'datetime',
    axisBorder: {
      show: false
    },
    // axisTicks: {
    //   show: false
    // },
  },
  yaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    labels: {
      formatter: (val) => {
        return val.toLocaleString();
      },
      style: {
        colors: '#78909c'
      }
    }
  },
  title: {
    text: 'Ventas por horario',
    align: 'left',
    style: {
      fontSize: '18px'
    }
  }

}