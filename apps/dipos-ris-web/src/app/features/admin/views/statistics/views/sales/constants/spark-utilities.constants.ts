import { ChartOptions } from "@admin-shared/types/chart-type";

export const UtilitiesOptions: Partial<ChartOptions> = {
  chart: {
    id: 'sparkline3',
    // group: 'sparklines',
    type: 'area',
    height: 150,
    sparkline: {
      enabled: true
    },
  },
  stroke: {
    curve: 'straight'
  },
  fill: {
    opacity: 1,
  },
  series: [],
  // series: [{
  //   name: 'Utilidades',
  //   data: randomizeArray(sparklineData)
  // }],
  // labels: [...Array(24).keys()].map(n => `2018-09-0${n + 1}`),
  xaxis: {
    type: 'datetime',
  },
  yaxis: {
    min: 0
  },
  colors: ['#008FFB'],
  //colors: ['#5564BE'],
  title: {
    text: 'S/ 0.00',
    offsetX: 30,
    style: {
      fontSize: '24px',
      // cssClass: 'apexcharts-yaxis-title'
    }
  },
  subtitle: {
    text: 'Utilidades',
    offsetX: 30,
    style: {
      fontSize: '14px',
      // cssClass: 'apexcharts-yaxis-title'
    }
  }
}