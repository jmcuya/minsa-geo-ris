import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ResultsComponent } from './components/results/results.component';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { AuthChildsGuard } from './guards/auth-childs.guard';
import { NgApexchartsModule } from 'ng-apexcharts';
import { UbicaRisComponent } from './views/ubica-ris/ubica-ris.component';
import { UbicaNoRisComponent } from './views/ubica-no-ris/ubica-no-ris.component';
import { NominalizacionComponent } from './views/nominalizacion/nominalizacion.component';
import { TumbesRisComponent } from './views/tumbes-ris/tumbes-ris.component';
import { TacnaRisComponent } from './views/tacna-ris/tacna-ris.component';
import { MadreDeDiosRisComponent } from './views/madre-de-dios-ris/madre-de-dios-ris.component';
import { UcayaliRisComponent } from './views/ucayali-ris/ucayali-ris.component';
import { EquipoMultidiciplinarioComponent } from './views/equipo-multidiciplinario/equipo-multidiciplinario.component';
import { GeorismapComponent } from './components/georismap/georismap.component';

@NgModule({
  imports: [
    // CommonModule,
    AdminRoutingModule,
    AdminSharedModule,
    NgApexchartsModule
  ],
  declarations: [
    DashboardComponent,
    ResultsComponent,
    UbicaRisComponent,
    UbicaNoRisComponent,
    NominalizacionComponent,
    TumbesRisComponent,
    TacnaRisComponent,
    MadreDeDiosRisComponent,
    UcayaliRisComponent,
    EquipoMultidiciplinarioComponent,
    GeorismapComponent
  ],
  providers: [
    AuthChildsGuard
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminModule { }
