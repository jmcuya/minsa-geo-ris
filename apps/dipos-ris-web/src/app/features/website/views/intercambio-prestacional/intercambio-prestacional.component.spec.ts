import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercambioPrestacionalComponent } from './intercambio-prestacional.component';

describe('IntercambioPrestacionalComponent', () => {
  let component: IntercambioPrestacionalComponent;
  let fixture: ComponentFixture<IntercambioPrestacionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntercambioPrestacionalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercambioPrestacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
