import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRisComponent } from './dashboard-ris.component';

describe('DashboardRisComponent', () => {
  let component: DashboardRisComponent;
  let fixture: ComponentFixture<DashboardRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
