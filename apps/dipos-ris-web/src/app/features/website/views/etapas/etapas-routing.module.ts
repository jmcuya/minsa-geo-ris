import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtapasComponent } from './etapas.component';

const routes: Routes = [
  {
		path: '',
		component: EtapasComponent,
		//canActivateChild: [AuthChildsGuard],
		children: [
			{
				path: 'etapas-0',
				loadChildren: () => import('./etapa-0/etapa-0.module').then(m => m.Etapa0Module)
			},
      {
				path: 'etapas-1',
				loadChildren: () => import('./etapa-1/etapa-1.module').then(m => m.Etapa1Module)
			},
      {
				path: 'etapas-2',
				loadChildren: () => import('./etapa-2/etapa-2.module').then(m => m.Etapa2Module)
			},
      {
				path: 'etapas-3',
				loadChildren: () => import('./etapa-3/etapa-3.module').then(m => m.Etapa3Module)
			},
      {
				path: 'etapas-4',
				loadChildren: () => import('./etapa-4/etapa-4.module').then(m => m.Etapa4Module)
			},
		]
	}
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtapasRoutingModule { }
