import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PolGeneralGobiernoComponent } from './pol-general-gobierno/pol-general-gobierno.component';
import { PolNacMultSaludComponent } from './pol-nac-mult-salud/pol-nac-mult-salud.component';
import { Ley30885Component } from './ley30885/ley30885.component';
const routes: Routes = [


  { path: '', pathMatch: 'full', component: PolNacMultSaludComponent },
  {
    path: 'Politica-Nacional-Multisectorial-Salud',
    component: PolNacMultSaludComponent
  },
  {
    path: 'Politica-General-Gobierno',
    component: PolGeneralGobiernoComponent
  },
  {
    path: 'Ley30885',
    component: Ley30885Component
  }




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Fase1RoutingModule { }
