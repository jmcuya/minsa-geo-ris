import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocNormRelDimGobModRISComponent } from './doc-norm-rel-dim-gob-mod-ris.component';

describe('DocNormRelDimGobModRISComponent', () => {
  let component: DocNormRelDimGobModRISComponent;
  let fixture: ComponentFixture<DocNormRelDimGobModRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocNormRelDimGobModRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocNormRelDimGobModRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
