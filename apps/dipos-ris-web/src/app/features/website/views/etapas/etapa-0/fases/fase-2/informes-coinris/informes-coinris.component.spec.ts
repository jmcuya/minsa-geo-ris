import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesCOINRISComponent } from './informes-coinris.component';

describe('InformesCOINRISComponent', () => {
  let component: InformesCOINRISComponent;
  let fixture: ComponentFixture<InformesCOINRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesCOINRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesCOINRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
