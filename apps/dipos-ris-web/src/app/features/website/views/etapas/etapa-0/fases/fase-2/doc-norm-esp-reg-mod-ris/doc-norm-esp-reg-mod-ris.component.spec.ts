import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocNormEspRegModRISComponent } from './doc-norm-esp-reg-mod-ris.component';

describe('DocNormEspRegModRISComponent', () => {
  let component: DocNormEspRegModRISComponent;
  let fixture: ComponentFixture<DocNormEspRegModRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocNormEspRegModRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocNormEspRegModRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
