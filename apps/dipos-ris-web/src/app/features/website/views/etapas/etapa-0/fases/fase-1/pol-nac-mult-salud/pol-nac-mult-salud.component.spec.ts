import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolNacMultSaludComponent } from './pol-nac-mult-salud.component';

describe('PolNacMultSaludComponent', () => {
  let component: PolNacMultSaludComponent;
  let fixture: ComponentFixture<PolNacMultSaludComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolNacMultSaludComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolNacMultSaludComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
