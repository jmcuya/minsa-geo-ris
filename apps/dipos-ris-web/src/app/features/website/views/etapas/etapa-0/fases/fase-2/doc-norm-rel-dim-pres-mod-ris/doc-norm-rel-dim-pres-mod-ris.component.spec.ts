import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocNormRelDimPresModRISComponent } from './doc-norm-rel-dim-pres-mod-ris.component';

describe('DocNormRelDimPresModRISComponent', () => {
  let component: DocNormRelDimPresModRISComponent;
  let fixture: ComponentFixture<DocNormRelDimPresModRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocNormRelDimPresModRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocNormRelDimPresModRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
