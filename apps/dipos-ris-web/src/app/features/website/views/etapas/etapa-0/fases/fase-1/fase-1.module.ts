import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Fase1RoutingModule } from './fase-1-routing.module';
import { PolNacMultSaludComponent } from './pol-nac-mult-salud/pol-nac-mult-salud.component';
import { PolGeneralGobiernoComponent } from './pol-general-gobierno/pol-general-gobierno.component';
import { Ley30885Component } from './ley30885/ley30885.component';
import { FaseUnoComponent } from './fase-uno.component';


@NgModule({
  declarations: [
    FaseUnoComponent,
    PolNacMultSaludComponent,
    PolGeneralGobiernoComponent,
    Ley30885Component
  ],
  imports: [
    CommonModule,
    Fase1RoutingModule
  ]
})
export class Fase1Module { }
