import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Fase3RoutingModule } from './fase-3-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Fase3RoutingModule
  ]
})
export class Fase3Module { }
