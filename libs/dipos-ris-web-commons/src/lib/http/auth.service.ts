import { HttpBackend, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenDecoded } from '@dipos-ris-web/models';
import { environment } from 'apps/dipos-ris-web/src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {

  private apiBase = environment.apiURL;

  constructor(private http: HttpClient, private handler: HttpBackend) {
    // Estas consultas http no pasarán por el HttpInterceptor
    this.http = new HttpClient(this.handler);
  }

  signIn(body: { username: string, password: string }): Observable<TokenDecoded> {
    // const userData = `username=${body.username}&password=${body.password}&grant_type=password`;
    const userData = new HttpParams()
      .set('username', body.username)
      .set('password', body.password);

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post<TokenDecoded>(`${this.apiBase}/api/login`, userData.toString(), { headers }).pipe(
      // tap(next => { console.log(next); }),
      map((res: TokenDecoded) => res)
    );
  }

  refreshToken(refreshToken: string): Observable<TokenDecoded> {
    const userData = `refresh_token=${refreshToken}&grant_type=refresh_token`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post<TokenDecoded>(`${this.apiBase}/token`, userData, { headers }).pipe(
      map((res: TokenDecoded) => res)
    );
  }

  recovery(usuario: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(`${this.apiBase}/api/user/recovery-password`, `=${usuario}`, { headers });
  }

  getDataByDocumento(documento: string): Observable<any> {
    return this.http.get(`https://servicio.apirest.pe/user/getRucDni?documento=${documento}`);
  }

}
