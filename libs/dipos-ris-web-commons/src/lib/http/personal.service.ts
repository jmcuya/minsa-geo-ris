import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class PersonalService {

  private apiBase = `${environment.apiURL}/api`;

  constructor(private http: HttpClient) { }

  companies(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/companies`);
  }

  search(name: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/personal?name=${name}`);
  }

  byID(id: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/personal/${id}`);
  }

  register(person: any): Observable<number> {
    return this.http.post<number>(`${this.apiBase}/personal/register`, person);
  }

  remove(id: number): Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiBase}/personal/remove/${id}`);
  }

}