import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class ReportsService {
  private apiBase = `${environment.apiURL}`;

  constructor(private http: HttpClient) { }

  sales(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/sales?${params.toString()}`);
  }

  salesByProduct(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/sales-by-product?${params.toString()}`);
  }

  salesByCustomer(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/sales-by-customer?${params.toString()}`);
  }

  accountsByReceivable(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/accounts-by-receivable?${params.toString()}`);
  }

  accountsByPay(start: string, end: string, state?: number): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end)
      .set('state', state);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/accounts-by-pay?${params.toString()}`);
  }

  incomesVsExpenses(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/incomes-vs-expenses?${params.toString()}`);
  }

  utilitiesBySale(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/utilities-by-sale?${params.toString()}`);
  }

  inventory(category?: number): Observable<any[]> {
    const params = new HttpParams()
      .set('category', category);

    return this.http.get<any[]>(`${this.apiBase}/api/reports/inventory?${params.toString()}`);
  }
}