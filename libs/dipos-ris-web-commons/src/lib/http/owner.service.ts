import { HttpBackend, HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class OwnerService {

  private apiBase = `${environment.apiURL}/api`;

  constructor(private http: HttpClient, private handler: HttpBackend) { }

  getAll(request?: any): Observable<any[]> {
    const params = new HttpParams()
      .set('type', request?.type || '')
      .set('param_type', request?.param_type || '')
      .set('param_text', request?.param_text || '');

    return this.http.get<any[]>(`${this.apiBase}/owners?${params.toString()}`);
  }

  byId(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/owners/${id}`);
  }

  register(owner: any): Observable<number> {
    return this.http.post<number>(`${this.apiBase}/owners`, owner);
  }

  getDataByDocument(document: string): Observable<any> {
    // const http = new HttpClient(this.handler);
    return this.http.get(`${this.apiBase}/owners/sunat?ruc=${document}`);
  }

}
