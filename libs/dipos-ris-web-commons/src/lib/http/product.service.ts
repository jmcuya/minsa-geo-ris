import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { iNominalizationRequest } from '../../../../../apps/dipos-ris-web/src/app/features/admin/interfaces/nominalizationRequest.interface';

@Injectable()
export class ProductService {

  private apiBase = environment.apiURL;

  constructor(private http: HttpClient) { }

  search(values: any): Observable<any[]> {
    const params = new HttpParams()
      .set('name', values.keywords)
      .set('sort', values.sort);

    return this.http.get<any[]>(`${this.apiBase}/api/products`, { params })
      .pipe(
        map(res => res.map(p => {
          p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
          return p;
        })
        )
      );
  }

  paging(values: any): Observable<any> {
    const params = new HttpParams()
      .set('size_page', values.size || 12)
      .set('page', values.page || 1)
      .set('id_category', values.category || null)
      .set('id_trademark', values.trademark || null)
      .set('name', values.keywords || '')
      .set('sort', values.sort || 'nombre')
      .set('id_warehouse', values.almacenId || null)
      .set('incUnits', values.flagStock || null);

    return this.http.get<any>(`${this.apiBase}/api/products/paging`, { params })
      .pipe(
        map(res => {
          res.data = res.data.map((p: any) => {
            p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
            return p;
          });

          return res;
        }
        )
      );
  }

  getItem(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/api/products/${id}`)
      .pipe(
        map(p => {
          p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
          return p;
        })
      );
  }

  units(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/products/units`);
  }

  unitsByProduct(id: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/products/${id}/units`);
  }

  categories(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/products/categories`);
  }

  trademarks(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/ris`);
  }

  resultados(ris: iNominalizationRequest, typeFilter:number): Observable<any[]> {
    return this.http.post<any[]>(`${this.apiBase}/api/nominalizacion?type=${typeFilter}`, ris);
  }

  trademarkInsert(trademark: any): Observable<number> {
    return this.http.post<number>(`${this.apiBase}/api/products/trademarks`, trademark);
  }

  insert(product: any): Observable<number> {
    const formData = new FormData();
    formData.append('file', product.file);

    delete product.image;
    delete product.file;

    formData.append('producto', JSON.stringify(product));
    return this.http.post<number>(`${this.apiBase}/api/products`, formData);
  }

}
