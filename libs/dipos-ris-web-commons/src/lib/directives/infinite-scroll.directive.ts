import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[infinite-scroll]'
})
export class InfiniteScrollDirective {

  constructor(private elementRef: ElementRef) { }

  @Output() track: EventEmitter<Event> = new EventEmitter();

  @HostListener('scroll', ['$event'])
  private onScroll($event: Event): void {
    const raw = this.elementRef.nativeElement;

    if (raw.scrollTop + raw.offsetHeight + 1 >= raw.scrollHeight) {
      this.track.emit($event);
    }
  };

}