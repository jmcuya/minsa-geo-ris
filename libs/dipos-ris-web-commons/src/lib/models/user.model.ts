export enum UserRol {
  admin = "ADMINISTRADOR",
  seller = "VENDEDOR"
}

export interface TokenDecoded {
  access_token: string;
  refresh_token: string;
  token_type: string;
  expires_in: number;
  refresh_expires_in: number;
  '.issued': string;
  '.expires': string;
  username: string;
  roles: UserRol;
  nombre: string;
}

export class TokenModel {
  access_token: string;
  refresh_token: string;
  token_type: string;
  expires: string;
  issued: string;
  expires_in: number;
  refresh_expires_in: number;

  constructor(tokenDecoded: TokenDecoded) {
    this.access_token = tokenDecoded.access_token;
    this.refresh_token = tokenDecoded.refresh_token;
    this.token_type = tokenDecoded.token_type;
    this.expires = tokenDecoded['.expires'];
    this.issued = tokenDecoded['.issued'];
    this.expires_in = tokenDecoded.expires_in;
    this.refresh_expires_in = tokenDecoded.refresh_expires_in;
  }

  get isExpired(): boolean {
    if (!this.issued) return true;
    const date = new Date(this.issued);
    return new Date(date.getTime() + (this.refresh_expires_in || this.expires_in) * 1000) < new Date();

    // if (!this.issued) return true;
    // const date = new Date(this.issued); // console.log(date);
    // return new Date(date.setDate(date.getDate() + 30)) < new Date();
    // // return this.expires && (new Date(this.expires) < new Date());
  }
}

export class UserModel {
  token: TokenModel;
  username: string;
  userRol: UserRol;
  nombre: string;

  constructor(tokenDecoded?: TokenDecoded) {
    if (tokenDecoded) {
      this.token = new TokenModel(tokenDecoded);
      this.username = tokenDecoded.username || '';
      this.userRol = tokenDecoded.roles as UserRol || null;
      this.nombre = tokenDecoded.nombre || null;
    }
  }

  isAdmin() {
    return this.userRol === UserRol.admin;
  }

  isSeller() {
    return this.userRol === UserRol.seller;
  }

  hasRole(rols: UserRol[]) {
    return rols.includes(this.userRol);
  }
}
